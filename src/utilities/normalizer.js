// @flow
/* eslint-disable no-nested-ternary */

import { Car } from 'src/types';

/**
 * Normalizes response JSON of two different formats
 *
 * @param response
 * @returns {*}
 */
export const normalize = (response: any) => {
  if (!response || !response.data) {
    return null;
  }

  const { data } = response;
  const result = {};

  // car2go
  if (data.placemarks) {
    data.placemarks.forEach(item => {
      const {
        id,
        coordinates,
        name,
        interior,
        exterior,
        address,
        fuel = 0
      } = item;

      const car: Car = {
        id,
        taxi: false,
        lnglat: [coordinates[0], coordinates[1]],
        status: interior === 'GOOD' && exterior === 'GOOD' ? 'Good' :
          interior === 'UNACCEPTABLE' && exterior === 'UNACCEPTABLE' ? 'Poor' :
          'Fair',
        address,
        fuel,
        plate: name,
      }

      result[id] = car;
    });
  }

  // mytaxi
  else if (data.poiList) {
    data.poiList.forEach(item => {
      const {
        id,
        coordinate,
        state,
      } = item;

      const car: Car = {
        id,
        taxi: true,
        lnglat: [coordinate.longitude, coordinate.latitude],
        status: state === 'ACTIVE' ? 'Available' : 'Occupied',
        address: null,
        fuel: 0,
        plate: null,
      }

      result[id] = car;
    });
  }

  return { cars: result };
}
