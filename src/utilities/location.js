// @flow

import { Position } from 'src/types';
import { MAP_LOCATION } from 'src/constants';

/**
 * Extract coordinates and return the result in the form [longitude, latitude]
 *
 * @returns {number[longitude,latitude]}
 */
const getCoordinates = ({
  coords: {
    latitude,
    longitude
  }
}: Position): Object => ({
  longitude,
  latitude
});

export const getDefaultLocation = () => {
  const [longitude, latitude] = MAP_LOCATION.split(',').map(parseFloat);

  return {
    latitude,
    longitude,
  }
};

/**
 * Tries to get user's location
 *
 * @returns {Promise}
 */
export const getLocation = () => {
  const { geolocation } = navigator;

  const promise: any = new Promise((resolve, reject) => {
    // console.log(geolocation);
    if (!geolocation) {
      reject(new Error('Geolocation not supported'));
    }

    geolocation.getCurrentPosition(
      (position: Position) => resolve({ location: getCoordinates(position) }),
      () => reject(new Error('Geolocation permission denied')),
    );
  });

  return promise;
}
