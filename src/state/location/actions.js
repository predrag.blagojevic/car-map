// @flow

import { createAction } from 'redux-actions';

export const FETCH_LOCATION = '[LOCATION] Fetch';
export const FETCH_LOCATION_SUCCESS = `${FETCH_LOCATION} success`;
export const FETCH_LOCATION_FAIL = `${FETCH_LOCATION} fail`;

export const SET_LOCATION = `[LOCATION] Set`;
export const SET_LOCATION_PIN = `${SET_LOCATION} pin`;

export const fetchLocation = createAction(FETCH_LOCATION);
export const fetchLocationSuccess = createAction(FETCH_LOCATION_SUCCESS);
export const fetchLocationFail = createAction(FETCH_LOCATION_FAIL);

export const setLocationPin = createAction(SET_LOCATION_PIN);
