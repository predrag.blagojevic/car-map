// @flow

import { handleActions } from 'redux-actions';

import { getDefaultLocation } from 'src/utilities';

import * as actions from './actions';
import initialState from './state';

export default handleActions(
  new Map([
    [
      actions.fetchLocation,
      state => ({
        ...state,
        inProgress: true,
      }),
    ],
    [
      actions.fetchLocationSuccess,
      (state, { payload }) => ({
        ...state,
        inProgress: false,
        location: payload.location || getDefaultLocation(),
      }),
    ],
    [
      actions.fetchLocationFail,
      (state, { payload }) => ({
        ...state,
        inProgress: false,
        error: payload && payload.message ? payload.message : null,
      }),
    ],
    [
      actions.setLocationPin,
      (state, { payload }) => ({
        ...state,
        pin: payload,
      }),
    ],
  ]),
  { ...initialState },
);
