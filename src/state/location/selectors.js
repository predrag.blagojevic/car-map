// @flow

import { createSelector } from 'reselect';
import { State } from './state';

export const getLocationState = ({ location }: State) =>
  location;
    
export const isFetchLocationInProgress = createSelector(
  getLocationState,
  ({ inProgress }: State) => inProgress,
);

export const getLocation = createSelector(
  getLocationState,
  ({ location }: State) => location,
);

export const getLocationPin = createSelector(
  getLocationState,
  ({ pin }: State) => pin,
);
