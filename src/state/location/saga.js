// @flow

import { all, takeEvery, put, call } from 'redux-saga/effects';

import { getLocation } from 'src/utilities';
import * as actions from './actions';

export function* fetchLocation$(): Generator<*, *, *> {
  try {
    const request = yield call(getLocation);
    yield put(actions.fetchLocationSuccess(request));
  }
  catch (e) {
    yield put(actions.fetchLocationFail());
  }
}

// $FlowIssue
export default function*() {
  yield all([takeEvery(actions.FETCH_LOCATION, fetchLocation$)]);
}
