// @flow

import { getDefaultLocation } from 'src/utilities';

export interface State {
  location: any;
  pin: any;
  inProgress: boolean;
  error: string | null;
};

const initialState: State = {
  location: getDefaultLocation(),
  pin: {},
  inProgress: false,
  error: null,
};

export default initialState;
