// @flow
import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { createBrowserHistory } from 'history';
import {
  connectRouter,
  routerMiddleware as createRouterMiddleware,
} from 'connected-react-router';
import { createLogger } from 'redux-logger';
import { composeWithDevTools } from 'redux-devtools-extension';

import rootReducer from './reducers';
import sagas from './sagas';
import { State } from './state';

const history = createBrowserHistory();

export default (initialState?: State) => {
  const sagaMiddleware = createSagaMiddleware();
  const routerMiddleware = createRouterMiddleware(history);
  const loggerMiddleware = createLogger({ level: 'info', collapsed: true });

  // Apply Middleware & Compose Enhancers
  const middleware = [sagaMiddleware, routerMiddleware, loggerMiddleware];

  const enhancers = [];
  enhancers.push(applyMiddleware(...middleware));

  // If Redux DevTools Extension is installed use it, otherwise use Redux compose
  /* eslint-disable no-underscore-dangle */
  const composeEnhancers = composeWithDevTools || compose;
  /* eslint-enable no-underscore-dangle */

  const enhancer = composeEnhancers(...enhancers);
  const connectedReducer = connectRouter(history)(rootReducer);
  const store = createStore(connectedReducer, initialState, enhancer);

  sagaMiddleware.run(sagas);

  return { store, history };
};
