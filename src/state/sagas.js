import { all } from 'redux-saga/effects';

import carsSaga from './cars/saga';
import locationSaga from './location/saga';

export default function*() {
  yield all([
    carsSaga(),
    locationSaga(),
  ]);
}
