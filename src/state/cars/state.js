// @flow

import { Filter } from 'src/types';

export interface State {
  cars: Object;
  filter: Filter;
  inProgress: boolean;
  error: string | null;
};

const initialState: State = {
  cars: {},
  filter: {
    car2go: true,
    mytaxi: true,
    bounds: null,
  },
  inProgress: false,
  error: null,
};

export default initialState;
