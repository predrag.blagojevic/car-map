// @flow
/* eslint-disable no-return-assign */

import { createSelector } from 'reselect';
import { Filter, Bounds } from 'src/types';

import { State } from './state';

export const getCarsState = ({ cars }: State) => cars;

export const isFetchCarsInProgress = createSelector(
  getCarsState,
  ({ inProgress }: State) => inProgress,
);

export const getCars = createSelector(
  getCarsState,
  ({ cars }: State) => cars,
);

export const getCarsBounds = createSelector(
  getCarsState,
  ({ filter: { bounds } }: State) => bounds,
);

export const getCarsByBounds = createSelector(
  getCarsState,
  getCarsBounds,
  ({ cars }: State, bounds: Bounds) => {
    if (!bounds) {
      return cars;
    }

    const result = {};

    Object.keys(cars).forEach((key: any) => {
      const car = cars[key];
      const lng = car.lnglat[0];
      const lat = car.lnglat[1];
      const { longitudes, latitudes } = bounds;

      if ((lng >= longitudes[0] && lng <= longitudes[1]) &&
        (lat >= latitudes[0] && lat <= latitudes[1])) {
        result[key] = cars[key];
      }
    });

    return result;
  },
);

export const getCarsFilter = createSelector(
  getCarsState,
  ({ filter }: State) => filter,
);

export const getCarsByFilter = createSelector(
  getCarsState,
  getCarsFilter,
  ({ cars }: State, filter: Filter) => {
    const { mytaxi, car2go } = filter;

    if (mytaxi && car2go) {
      return cars;
    }

    const result = {};

    Object.keys(cars).forEach((key: any) => {
      if (mytaxi && cars[key].taxi) {
        result[key] = cars[key];
      }

      if (car2go && !cars[key].taxi) {
        result[key] = cars[key];
      }
    });

    return result;
  }
);

export const getCarsByFilterAndBounds = createSelector(
  getCarsByFilter,
  getCarsByBounds,
  (byFilter: any, byBounds: any) => {
    const result = {};

    const byFilterKeys = (byFilter && Object.keys(byFilter)) || [];
    const byBoundsKeys = (byBounds && Object.keys(byBounds)) || [];

    const filtered = byFilterKeys.filter(id => byBoundsKeys.indexOf(id) !== -1);

    filtered.forEach(id => result[id] = byFilter[id] || byBounds[id]);

    return result;
  }
);
