// @flow

import { handleActions } from 'redux-actions';

import * as actions from './actions';
import initialState from './state';

export default handleActions(
  new Map([
    [
      actions.fetchCars,
      state => ({
        ...state,
        inProgress: true,
        error: null,
      }),
    ],
    [
      actions.fetchCarsSuccess,
      (state, { payload }) => {
        // console.log('handleActions', Object.keys(payload.cars).length);
        const cars = {
          ...state.cars,
          ...payload.cars
        }
        // console.log('handleActions', Object.keys(cars).length);
        return {
          ...state,
          cars,
          inProgress: false,
        }
      },
    ],
    [
      actions.fetchCarsFail,
      (state, { payload }) => ({
        ...state,
        inProgress: false,
        error: payload && payload.message ? payload.message : null,
      }),
    ],
    [
      actions.setCarsFilter,
      (state, { payload }) => ({
        ...state,
        filter: {
          ...state.filter,
          ...payload
        },
      }),
    ],
    [
      actions.setCarsBounds,
      (state, { payload }) => ({
        ...state,
        filter: {
          ...state.filter,
          bounds: payload,
        },
      }),
    ],
  ]),
  { ...initialState },
);
