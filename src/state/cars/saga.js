// @flow

import { all, takeEvery, put, fork } from 'redux-saga/effects';

import { http } from 'src/services/http';
import { normalize } from 'src/utilities';

import * as actions from './actions';


/**
 * Fetch from car2go endpoint
 *
 * @param response
 * @returns {IterableIterator<*|PutEffect<Action>>}
 */
export function* fetchCar2go$(response: any): Generator<*, *, *> {
  try {
    // console.log('fetchCar2go$', response.data);
    yield put(actions.fetchCarsSuccess(normalize(response)));
  }
  catch (e) {
    yield put(actions.fetchCarsFail());
  }
}

/**
 * Fetch from mytaxi endpoint
 *
 * @param response
 * @returns {IterableIterator<*|PutEffect<Action>>}
 */
export function* fetchMytaxi$(response: any): Generator<*, *, *> {
  try {
    // console.log('fetchMytaxi$', response.data);
    yield put(actions.fetchCarsSuccess(normalize(response)));
  }
  catch (e) {
    yield put(actions.fetchCarsFail());
  }
}

/**
 * Run two sagas in parallel
 *
 * @returns {IterableIterator<*>}
 */
export function* fetchCars$(): Generator<*, *, *> {
  try {
    yield fork(fetchCar2go$, yield http.get('/car2go/vehicles'));
    yield fork(fetchMytaxi$, yield http.get('/mytaxi/vehicles'));
  }
  catch (e) {
    yield put(actions.fetchCarsFail());
  }
}

// $FlowIssue
export default function*() {
  yield all([takeEvery(actions.FETCH_CARS, fetchCars$)]);
}

