// @flow

import { createAction } from 'redux-actions';

export const FETCH_CARS = `[CARS] Fetch`;
export const FETCH_CARS_SUCCESS = `${FETCH_CARS} success`;
export const FETCH_CARS_FAIL = `${FETCH_CARS} fail`;

export const SET_CARS = `[CARS] Set`;
export const SET_CARS_BOUNDS = `${SET_CARS} bounds`;
export const SET_CARS_FILTER = `${SET_CARS} filter`;

export const fetchCars = createAction(FETCH_CARS);
export const fetchCarsSuccess = createAction(FETCH_CARS_SUCCESS, payload => payload);
export const fetchCarsFail = createAction(FETCH_CARS_FAIL);

export const setCarsBounds = createAction(SET_CARS_BOUNDS);
export const setCarsFilter = createAction(SET_CARS_FILTER);
