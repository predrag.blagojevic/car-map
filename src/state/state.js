import { State as CarsState } from './cars/state';
import { State as LocationState } from './location/state';

export type StateT = {
  cars: CarsState,
  location: LocationState,
};
