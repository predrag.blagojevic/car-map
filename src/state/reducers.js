// @flow

import { combineReducers } from 'redux';
import { routerReducer as router } from 'react-router-redux';

import cars from './cars/reducer';
import location from './location/reducer';

export default combineReducers({
  cars,
  location,
  router,
});

