// @flow

export interface Car {
  id: number;
  taxi: boolean;
  lnglat: number[/* longitude, latitude */];
  status: 'GOOD' | 'FAIR' | 'POOR' | 'AVAILABLE' | 'OCCUPIED';
  address: string | null;
  fuel: number;
  plate: string | null;
}
