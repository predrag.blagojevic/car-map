// @flow

export interface Bounds {
  longitudes: number[/* east, west */];
  latitudes: number[/* east, west */];
}

export interface Filter {
  car2go: boolean;
  mytaxi: boolean;
  bounds: Bounds | null;
}

