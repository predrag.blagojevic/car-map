const env = process.env; // eslint-disable-line

const APP_PREFIX = 'MTX_';

export const API_URL: string = env[`${APP_PREFIX}API_URL`];

export const HTTP_RETRY_REQUESTS: boolean = Boolean(env[`${APP_PREFIX}HTTP_RETRY_REQUESTS`]);
export const HTTP_TIMEOUT: number = Number(env[`${APP_PREFIX}HTTP_TIMEOUT`]);

export const MAP_API_TOKEN: string = env[`${APP_PREFIX}MAPBOX_ACCESS_TOKEN`];
export const MAP_STYLE: string = env[`${APP_PREFIX}MAPBOX_STYLE`];
export const MAP_LOCATION: string = env[`${APP_PREFIX}MAPBOX_DEFAULT_LNGLAT`];
