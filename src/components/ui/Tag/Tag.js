// @flow
/* eslint-disable react/require-default-props */

import React from 'react';
import classNames from 'classnames';

import { THEME } from 'src/constants';

type TagTypes = 'success' | 'warning' | 'alert' | 'info' | 'active';

type PropsT = {
  children: any,
  type?: TagTypes,
  title?: string,
  className?: string,
};

const baseClass = `${THEME}-tag`;

const Tag = (props: PropsT) => {
  const { children, type = '', title, className } = props;

  const classes = classNames(
    baseClass,
    type && `${baseClass}--${type}`,
    className,
  );

  return <span className={classes} title={title}>{children}</span>;
};

export default Tag;
