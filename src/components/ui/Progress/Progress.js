// @flow

/* eslint-disable react/require-default-props */

import React from 'react';
import classNames from 'classnames';

import { THEME } from 'src/constants';

const baseClass = `${THEME}-progress`;

type ProgressSizes = 'tiny' | 'small' | 'full';

type PropsT = {
  size?: ProgressSizes,
  max?: number,
  value?: number,
  message?: string,
  title?: string,
  className?: string,
};

const Progress = (props: PropsT) => {
  const { size, value = 0, message, max = 100, title, className } = props;

  const classes = classNames(
    baseClass,
    size && `${baseClass}--${size}`,
    className,
  );

  return (
    <span className={classes} aria-label={message} title={title}>
      <progress max={max} value={value}>
        {value}%
      </progress>
    </span>
  );
};

export default Progress;
