// @flow
/* eslint react/sort-comp: 0 */

import React, { PureComponent } from 'react';
import isEmpty from 'lodash/isEmpty';

import { THEME } from 'src/constants';

import Loader from '@ui/Loader';
import Filter from '@views/Filter';
import Car from '@views/Car';

const baseClass = `${THEME}-sidebar`;

type Props = {
	cars: any;
	fetchCarsInProgress: boolean;
	fetchCars: Function;
	fetchLocation: Function;
  setLocationPin: Function;
};

class Sidebar extends PureComponent<Props> {
  componentDidMount() {
    const { fetchCars } = this.props;

    fetchCars();
  }

  handleClick = (location: number[]) => {
    this.props.setLocationPin(location);
  }

  renderHeader = () => (
    <header className={`${baseClass}__header`}>
      <Filter />
    </header>
  );

  renderItems = (items: any) => (
    <ol className={`${THEME}-list`}>
      {Object.values(items).map((car: Car) => (
        <li key={car.id} className={`${THEME}-list__item`}>
          <Car {...car} onClick={this.handleClick} />
        </li>
      ))}
    </ol>
  );

  renderFooter = (items: any) => {
    const values = Object.values(items);
    const mytaxi: number = items ? values.filter((item: Car) => item.taxi).length : 0;
    const car2go: number = items ? values.filter((item: Car) => !item.taxi).length : 0;
    const total: number = items ? values.length : 0;

    return (
      <footer className={`${baseClass}__footer`}>
        <h5>Number of vehicles:</h5>
        <p>mytaxi: <b>{mytaxi}</b></p>
        <p>car2go: <b>{car2go}</b></p>
        <p>Total: <b>{total}</b></p>
      </footer>
    );
  }

  render() {
    const { fetchCarsInProgress, cars } = this.props;

    return (
      <aside className={baseClass}>
        {this.renderHeader()}

        <section className={`${baseClass}__content`}>
          {fetchCarsInProgress && <Loader />}
          {!isEmpty(cars) ? this.renderItems(cars) : (
            <div style={{ margin: 'auto' }}>No cars :(</div>
          )}
        </section>

        {this.renderFooter(cars)}
      </aside>
    );
  }
}

export default Sidebar;
