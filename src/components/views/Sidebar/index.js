// @flow

import { connect } from 'react-redux';
import { compose } from 'recompose';

import { fetchCars, fetchLocation, setLocationPin } from 'src/state/actions';
import { isFetchCarsInProgress, getCarsByFilter } from 'src/state/selectors';

import Sidebar from './Sidebar';

const actions = {
  fetchCars,
  fetchLocation,
  setLocationPin,
};

const mapStateToProps = state => ({
  cars: getCarsByFilter(state),
  fetchCarsInProgress: isFetchCarsInProgress(state),
});

export default compose(
  connect(
    mapStateToProps,
    actions,
  ),
)(Sidebar);
