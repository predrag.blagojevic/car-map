// @flow
/* eslint-disable no-shadow */

import { connect } from 'react-redux';
import { compose } from 'recompose';

import { fetchLocation, setCarsBounds } from 'src/state/actions';
import { getLocation, getCarsByFilterAndBounds, isFetchLocationInProgress, getLocationPin } from 'src/state/selectors';

import Map from './Map';

const actions = {
  setCarsBounds,
  fetchLocation,
};

const mapStateToProps = state => ({
  cars: getCarsByFilterAndBounds(state),
  location: getLocation(state),
  fetchLocationInProgress: isFetchLocationInProgress(state),
  pin: getLocationPin(state),
});

export default compose(
  connect(
    mapStateToProps,
    actions,
  ),
)(Map);
