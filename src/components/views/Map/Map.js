// @flow

import React, { PureComponent } from 'react';
import MapGL, { Popup, NavigationControl } from 'react-map-gl';
import debounce from 'lodash/debounce';

import { Car } from 'src/types';

import { THEME, MAP_API_TOKEN, MAP_STYLE } from 'src/constants';

import Marker from './Marker';

interface Props {
  cars: any;
  location: any;
  pin: number[];
  fetchLocationInProgress: boolean;
  setCarsBounds: Function;
  fetchLocation: Function;
}

interface State {
  viewport: any;
  popupInfo: any;
  isLoaded: boolean;
}

const baseClass = `${THEME}-map`;

class Map extends PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      viewport: {
        zoom: 16,
        bearing: 0,
        pitch: 0,
      },
      popupInfo: null,
      isLoaded: false,
    };

    this.debouncedViewportChange = debounce(this.handleViewportChange, 100);

    this.mapRef = React.createRef();
  }

  componentDidMount() {
    const { fetchLocation } = this.props;

    fetchLocation();
  }

  componentDidUpdate(prevProps: Props) {
    if (prevProps.pin !== this.props.pin) {
      this.handleViewportChange(this.state.viewport);
    }
  }

  mapRef: any;
  debouncedViewportChange: Function;

  handleViewportChange = (viewport: any) => {
    const { setCarsBounds } = this.props;

    const map = this.mapRef.current.getMap();

    const { _ne, _sw } = map.getBounds();
    const bounds = {
      longitudes: [_sw.lng, _ne.lng],
      latitudes: [_sw.lat, _ne.lat],
    }
    // console.log('handleViewportChange', viewport);

    this.setState({ viewport }, () => {
      setCarsBounds(bounds);
    });
  }

  handleMapLoad = () => {
    // console.log('handleMapLoad', e.target.getBounds());
    this.setState({ isLoaded: true });
  }

  renderMarkers = () => {
    const { cars } = this.props;
    // console.log('renderMarkers', Object.keys(cars).length);

    // $FlowIssue
    return Object.values(cars).map((car: Car) => (
      <Marker
        key={car.id}
        data={car}
        onClick={(info: any) => this.setState({
          popupInfo: {
            content: info.content,
            longitude: info.longitude,
            latitude: info.latitude,
          }
        })}
      />
    ));
  }

  renderPopup = () => {
    const { popupInfo } = this.state;

    return popupInfo && (
      <Popup
        anchor="top-left"
        longitude={popupInfo.longitude}
        latitude={popupInfo.latitude}
        tipSize={5}
        offsetTop={5}
        onClose={() => this.setState({ popupInfo: null })}
      >
        <div className={`${baseClass}__popup`}>
          {popupInfo.content}
        </div>
      </Popup>
    );
  }

  render() {
    const { viewport, isLoaded } = this.state;
    const { location, pin } = this.props;

    return (
      <div className={baseClass}>
        <MapGL
          {...viewport}
          {...location}
          {...pin}
          width="100%"
          height="100%"
          ref={this.mapRef}
          mapStyle={MAP_STYLE}
          logoPosition={null}
          attributionControl={false}
          dragPan
          mapboxApiAccessToken={MAP_API_TOKEN}
          onViewportChange={this.debouncedViewportChange}
          onLoad={this.handleMapLoad}
        >
          <div className={`${baseClass}__navigation`}>
            <NavigationControl onViewportChange={this.handleViewportChange} />
          </div>
          {isLoaded && this.renderMarkers()}
          {isLoaded && this.renderPopup()}
        </MapGL>
      </div>
    );
  }
}

export default Map;
