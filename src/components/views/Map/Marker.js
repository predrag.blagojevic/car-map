// @flow

import React from 'react';
import { Marker as MapGLMarker } from 'react-map-gl';

import { THEME } from 'src/constants';
import { Car } from 'src/types';

import CarSVG from '@images/icon-car-pin.svg';
import TaxiSVG from '@images/icon-taxi-pin.svg';
import Icon from '@ui/Icon';

const baseClass = `${THEME}-marker`;

interface Props {
  data: Car;
  onClick: Function;
}

const Marker = (props: Props) => {
  const { data, onClick = f => f } = props;

  const {
    taxi,
    lnglat,
    address,
    status,
    fuel,
    plate,
  } = data;

  const icon: any =  taxi ? TaxiSVG : CarSVG;

  const metaClass = `${baseClass}__meta`;

  const content: any = taxi ? (
    <dl className={`${metaClass}`}>
      <dt className={`${metaClass}__title`}>mytaxi</dt>
      <dd className={`${metaClass}__data`}>Status: <b>{status}</b></dd>
      <dd className={`${metaClass}__data`}>Location:</dd>
      <dd className={`${metaClass}__data`}><small>{lnglat[1]}, {lnglat[0]}</small></dd>
    </dl>
  ) : (
    <dl className={`${metaClass}`}>
      <dt className={`${metaClass}__title`}>car2go</dt>
      <dd className={`${metaClass}__data`}>Plate nr: <b>{plate}</b></dd>
      <dd className={`${metaClass}__data`}>Car condition:<b>{status}</b></dd>
      <dd className={`${metaClass}__data`}>Fuel level: <b>{fuel}%</b></dd>
      <dd className={`${metaClass}__data`}>Address:</dd>
      <dd className={`${metaClass}__data`}><small>{address}</small></dd>
      <dd className={`${metaClass}__data`}><small>{lnglat[1]}, {lnglat[0]}</small></dd>
    </dl>
  );

  const handleClick = () => onClick({
    content,
    longitude: lnglat[0],
    latitude: lnglat[1],
  });

  return (
    <MapGLMarker
      longitude={lnglat[0]}
      latitude={lnglat[1]}
      offsetLeft={-16}
      offsetTop={-32}
    >
      <Icon
        data={icon}
        size="pin"
        color
        onClick={handleClick}
        className={baseClass}
      />
    </MapGLMarker>
  );
}

export default Marker;
