// @flow
/* eslint-disable jsx-a11y/click-events-have-key-events */

import * as React from 'react';
import classNames from 'classnames';

import { THEME } from 'src/constants';
import { Car as CarType } from 'src/types';

import CarSVG from '@images/icon-car.svg';
import TaxiSVG from '@images/icon-taxi.svg';
import Icon from '@ui/Icon';
import Tag from '@ui/Tag';
import Progress from '@ui/Progress';

const baseClass = `${THEME}-car`;

interface Props extends CarType {
  className?: string;
  onClick: Function;
}

const getStatusColor = (status: string): string => {
  switch (status.toLowerCase()) {
    case 'poor':
      return 'alert';
    case 'fair':
      return 'warning';
    case 'good':
      return 'success';
    case 'available':
      return 'info';
    default:
      return '';
  }
}

const Car = (props: Props) => {
  const {
    taxi,
    lnglat,
    status,
    address,
    fuel = 0,
    plate,
    className,
    onClick = f => f,
  } = props;

  const classes = classNames(
    baseClass,
    className,
  );

  const handleClick = () => onClick({ longitude: lnglat[0], latitude: lnglat[1] });

  return (
    <div className={classes} onClick={handleClick} role="presentation">
      <Icon
        data={taxi ? TaxiSVG : CarSVG}
        size="toolbar"
        spacing="right"
        color
        title={taxi ? 'mytaxi' : 'car2go'}
        className={`${baseClass}__icon`}
      />
      <section className={`${baseClass}__details`} title={`Coordinates: ${lnglat[1]}, ${lnglat[0]}`}>
        {taxi ? <span className={`${baseClass}__location`}>{`${lnglat[1]}, ${lnglat[0]}`}</span> :
          <span className={`${baseClass}__plate`}>{plate}</span>}
        <address className={`${baseClass}__address`}>{address}</address>
      </section>
      <section className={`${baseClass}__info`}>
        <Tag
          type={getStatusColor(status)}
          title={taxi ? `Taxi is ${status.toLowerCase()}` : `Car condition is ${status.toLowerCase()}`}
          className={`${baseClass}__status`}
        >
          {status}
        </Tag>
        {!taxi && (
          <Progress
            size="tiny"
            value={fuel}
            title={`Fuel level: ${fuel}%`}
            className={`${baseClass}__fuel`}
          />
        )}
      </section>
    </div>
  );
};

export default Car;
