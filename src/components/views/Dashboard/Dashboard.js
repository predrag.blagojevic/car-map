// @flow

import React from 'react';

import Sidebar from '@views/Sidebar';
import Map from '@views/Map';

import { THEME } from 'src/constants';

const baseClass = `${THEME}-dashboard`;

const Dashboard = () => (
  <div className={baseClass}>
		<Sidebar />
		<Map />
  </div>
);

export default Dashboard;
