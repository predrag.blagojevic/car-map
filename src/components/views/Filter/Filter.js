// @flow
/* eslint-disable jsx-a11y/label-has-for */

import * as React from 'react';
import classNames from 'classnames';

import MytaxiSVG from '@images/mytaxi-logo.svg';
import Car2goSVG from '@images/car2go-logo.svg';
import Checkbox from '@ui/Checkbox';

import { THEME } from 'src/constants';

const baseClass = `${THEME}-filter`;

interface Props {
  filter: any;
  setCarsFilter: Function;
}

const Filter = (props: Props) => {
  const {
    filter,
    setCarsFilter,
  } = props;

  const classes = classNames(
    baseClass,
  );

  const { car2go, mytaxi } = filter;

  const handleChange = ({ value, checked }) => setCarsFilter({ [value]: checked });

  return (
    <div className={classes}>
      <label>
        <Checkbox checked={mytaxi} value="mytaxi" onChange={handleChange} />
        <MytaxiSVG />
      </label>
      <label>
        <Checkbox checked={car2go} value="car2go" onChange={handleChange} />
        <Car2goSVG />
      </label>
    </div>
  );
};

export default Filter;
