// @flow

import { connect } from 'react-redux';
import { compose } from 'recompose';

import { setCarsFilter } from 'src/state/actions';
import { getCarsFilter } from 'src/state/selectors';

import Filter from './Filter';

const actions = {
  setCarsFilter,
};

const mapStateToProps = state => ({
  filter: getCarsFilter(state),
});

export default compose(
  connect(
    mapStateToProps,
    actions,
  ),
)(Filter);
