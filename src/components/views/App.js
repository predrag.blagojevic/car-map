import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';

import { DASHBOARD_PAGE, THEME } from 'src/constants';
import { StateT } from 'src/state/state';

import Dashboard from './Dashboard';

const baseClass = `${THEME}-app`;

const App = ({ store, history }: { store: StateT, history: Object }) => (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <div className={baseClass}>
        <Switch className={`${baseClass}__content`}>
          <Route exact path="/" component={() => <Redirect to={DASHBOARD_PAGE} />} />
          <Route path={DASHBOARD_PAGE} component={Dashboard} />
        </Switch>
      </div>
    </ConnectedRouter>
  </Provider>
);
export default App;
